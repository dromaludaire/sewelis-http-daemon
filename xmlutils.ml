module Sewelis = Func_api

let send_xml = fun env cgi xml ->
  let xml_declaration = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" in
  let response = xml_declaration ^ (Xml.to_string xml) in
  env # set_output_header_field "Content-Type" "text/xml";
  cgi # output # output_string response;
  cgi # output # commit_work()



(* -------------------------------------------------------------- *)
(* Safety wrt. special charaters in strings *)

let xml_string = Netencoding.Html.encode
  ~in_enc:`Enc_utf8
  ~out_enc:`Enc_utf8
  ~unsafe_chars:"<>&\"'" ()

let url_decode = Netencoding.Url.decode

let pcdata = fun s ->
  Xml.PCData (xml_string s)


(* -------------------------------------------------------------- *)
(* reading parameters from http command *)

let int_argument = fun (cgi: Netcgi.cgi) name ->
  int_of_string (cgi # argument_value name)

let string_argument = fun (cgi: Netcgi.cgi) name ->
  url_decode (cgi # argument_value name)

let bool_argument = fun (cgi: Netcgi.cgi) name ->
  bool_of_string (cgi # argument_value name)


(* -------------------------------------------------------------- *)
(* Helper functions for building xml answers *)

let xml_ok = fun ?(attrs = []) ?(children = []) tag ->
  let safe_attrs = List.map (fun (x, y) -> x, xml_string y) attrs in
  let attrs' = ("status", "ok") :: safe_attrs in
  Xml.Element (tag, attrs', children)


let xml_error = fun tag xml_msg ->
  Xml.Element (tag,
               ["status", "error"],
               xml_msg)


(* -------------------------------------------------------------- *)
(* Convert Sewelis structures to xml *)

let xml_of_ns = fun (prefix, ns) ->
    Xml.Element ("namespaceDefinition", ["prefix", prefix; "namespace", ns], [])


let xml_of_column = fun place column ->
  let hidden = string_of_bool (Sewelis.column_hidden ~place ~column) in
  (* TODO: add other properties: order, pattern, aggreg *)
  let order = match Sewelis.column_order ~place ~column with
    | `DEFAULT -> "default"
    | `DESC -> "desc"
    | `ASC -> "asc" in
  let pattern = Sewelis.column_pattern ~place ~column in
  let aggreg = match Sewelis.column_aggreg ~place ~column with
    | `INDEX -> "index"
    | `DISTINCT_COUNT -> "distinct_count"
    | `SUM -> "sum"
    | `AVG -> "avg" in
  let attrs = ["hidden", hidden;
               "order", order;
               "pattern", pattern;
               "aggreg", aggreg] in
  Xml.Element ("column", attrs, [pcdata column])


let xml_of_cell = fun cell_id ->
  let attrs = ["cellId", string_of_int cell_id] in
  Xml.Element ("cell", attrs, [])


let xml_of_cell_row = fun row ->
  let rows = List.map xml_of_cell row in
  Xml.Element ("row", [], rows)


let xml_of_increment = fun id ->
  Xml.Element ("increment", ["id", string_of_int id], [])


let rec xml_of_increment_tree = fun incr_tree ->
  match incr_tree with
  | Sewelis.Node (id, []) -> xml_of_increment id
  | Sewelis.Node (id, subtrees) ->
     Xml.Element ("increment", ["id", string_of_int id],
                  List.map xml_of_increment_tree subtrees)


let xml_of_completions = fun completions ->
  let attrs = ["partial", string_of_bool completions.Sewelis.partial;
               "relaxed", string_of_bool completions.Sewelis.relaxed] in
  let incrs = Xml.Element ("increments", [],
                           List.map xml_of_increment completions.increments) in
  Xml.Element ("completions", attrs, [incrs])


let xml_of_term = fun t ->
  match t with
  | Rdf.URI uri -> Xml.Element ("URI", [], [pcdata uri])
  | Rdf.XMLLiteral xml -> Xml.Element ("XMLLiteral", [], [xml])
  | Rdf.Literal (s, Rdf.Plain "") -> Xml.Element ("Literal", [], [pcdata s])
  | Rdf.Literal (s, Rdf.Plain lang) -> Xml.Element ("Literal", ["xml:lang", lang], [pcdata s])
  | Rdf.Literal (s, Rdf.Typed dt) -> Xml.Element ("Literal", ["rdf:datatype", dt], [pcdata s])
  | Rdf.Blank id -> Xml.Element ("Blank", [], [pcdata id])


let string_of_uri_kind = fun k ->
  match k with
  | `Entity -> "entity"
  | `Class -> "class"
  | `Property -> "property"
  | `Functor -> "functor"
  | `Datatype -> "datatype"

let rec xml_of_display = fun l ->
  Xml.Element ("display", [], (List.map xml_of_token l))
and xml_of_token = fun token ->
  match token with
  | `Space -> Xml.Element ("Space", [], [])
  | `Kwd s -> Xml.Element ("Kwd", [], [pcdata s])
  | `Var v -> Xml.Element ("Var", [], [pcdata v])
  | `URI (uri, kind, s, None) ->
     Xml.Element ("URI", ["uri", uri; "kind", string_of_uri_kind kind], [pcdata s])
  | `URI (uri, kind, s, Some img_uri) ->
     Xml.Element ("URI",
                  ["uri", uri; "kind", string_of_uri_kind kind; "image", img_uri],
                  [pcdata s])
  | `Prim s -> Xml.Element ("Prim", [], [pcdata s])
  | `Plain (s, "") -> Xml.Element ("Plain", [], [pcdata s])
  | `Plain (s, lang) -> Xml.Element ("Plain", ["xml:lang", lang], [pcdata s])
  | `Typed (s, uri, "") -> Xml.Element ("Typed", ["uri", uri], [pcdata s])
  | `Typed (s, uri, s_dt) ->
     Xml.Element ("Typed", ["uri", uri; "datatype", s_dt], [pcdata s])
  | `Xml xml -> Xml.Element ("Xml", [], [xml])
  | `List ltoks -> Xml.Element ("List", [], List.map xml_of_display ltoks)
  | `Tuple ltoks -> Xml.Element ("Tuple", [], List.map xml_of_display ltoks)
  | `Seq ltoks -> Xml.Element ("Seq", [], List.map xml_of_display ltoks)
  | `And ltoks -> Xml.Element ("And", [], List.map xml_of_display ltoks)
  | `Or ltoks -> Xml.Element ("Or", [], List.map xml_of_display ltoks)
  | `Not toks -> Xml.Element ("Not", [], [xml_of_display toks])
  | `Maybe toks -> Xml.Element ("Maybe", [], [xml_of_display toks])
  | `Brackets toks -> Xml.Element ("Brackets", [], [xml_of_display toks])
  | `Quote toks -> Xml.Element ("Quote", [], [xml_of_display toks])
  | `Pair (force_indent, toks1, toks2) ->
     Xml.Element ("Pair",
                  ["forceIndent", string_of_bool force_indent],
                  [xml_of_display toks1; xml_of_display toks2])
  | `Focus (foc, toks) -> Xml.Element ("Focus",
                                       ["id", string_of_int foc],
                                       [xml_of_display toks])


let xml_of_transformation = fun t ->
  let open Lisql.Transf in
  match t with
  | FocusUp -> Xml.Element ("FocusUp", [], [])
  | FocusDown -> Xml.Element ("FocusDown", [], [])
  | FocusLeft -> Xml.Element ("FocusLeft", [], [])
  | FocusRight -> Xml.Element ("FocusRight", [], [])
  | FocusTab -> Xml.Element ("FocusTab", [], [])
  | InsertForEach -> Xml.Element ("InsertForEach", [], [])
  | InsertCond -> Xml.Element ("InsertCond", [], [])
  | InsertSeq -> Xml.Element ("InsertSeq", [], [])
  | InsertAnd -> Xml.Element ("InsertAnd", [], [])
  | InsertOr -> Xml.Element ("InsertOr", [], [])
  | InsertAndNot -> Xml.Element ("InsertAndNot", [], [])
  | InsertAndMaybe -> Xml.Element ("InsertAndMaybe", [], [])
  | InsertIsThere -> Xml.Element ("InsertIsThere", [], [])
  | ToggleQu An -> Xml.Element ("ToggleQu", [], [Xml.Element ("An", [], [])])
  | ToggleQu The -> Xml.Element ("ToggleQu", [], [Xml.Element ("The", [], [])])
  | ToggleQu Every -> Xml.Element ("ToggleQu", [], [Xml.Element ("Every", [], [])])
  | ToggleQu Only -> Xml.Element ("ToggleQu", [], [Xml.Element ("Only", [], [])])
  | ToggleQu No -> Xml.Element ("ToggleQu", [], [Xml.Element ("No", [], [])])
  | ToggleQu Each -> Xml.Element ("ToggleQu", [], [Xml.Element ("Each", [], [])])
  | ToggleNot -> Xml.Element ("ToggleNot", [], [])
  | ToggleMaybe -> Xml.Element ("ToggleMaybe", [], [])
  | ToggleOpt -> Xml.Element ("ToggleOpt", [], [])
  | ToggleTrans -> Xml.Element ("ToggleTrans", [], [])
  | ToggleSym -> Xml.Element ("ToggleSym", [], [])
  | Describe -> Xml.Element ("Describe", [], [])
  | Select -> Xml.Element ("Select", [], [])
  | Delete -> Xml.Element ("Delete", [], [])


exception Not_a_valid_rdf_term of string

let rdf_of_xmlstring = fun s ->
  let xml = Xml.parse_string s in
  match xml with
  | Xml.Element ("URI", [], [Xml.PCData uri]) -> Rdf.URI uri
  | Xml.Element ("XMLLiteral", [], [Xml.PCData x]) ->
     Rdf.XMLLiteral (Xml.parse_string x)
  | Xml.Element ("Literal", [], [Xml.PCData s]) ->
     Rdf.Literal (s, Rdf.Plain "")
  | Xml.Element ("Literal", ["xml:lang", lang], [Xml.PCData s]) ->
     Rdf.Literal (s, Rdf.Plain lang)
  | Xml.Element ("Literal", ["rdf:datatype", dt], []) ->
     Rdf.Literal (s, Rdf.Typed dt)
  | Xml.Element ("Blank", [], [Xml.PCData id]) -> Rdf.Blank id
  | _ -> raise (Not_a_valid_rdf_term s)
