(* Function application a la haskell *)
let ($) = fun f x -> f x


let uncurry f (x, y) = f x y


let (//) = Filename.concat


exception No_value

let val_of_option = fun x ->
  match x with
  | Some s -> s
  | None -> raise No_value

let string_of_option = fun x ->
  match x with
  | Some s -> s
  | None -> ""

let is_some x = x <> None


(* Taken from Batteries.Array *)
let array_mem a xs =
  let n = Array.length xs in
  let rec loop i =
    if i = n then false
    else if a = xs.(i) then true
    else loop (succ i)
  in
  loop 0


(* Taken from ExtLib.extString *)
let starts_with str p =
  if String.length str < String.length p then false
  else
    let rec loop str p i =
      if i = String.length p then true else
        if String.unsafe_get str i <> String.unsafe_get p i then false
        else loop str p (i+1)
    in
    loop str p 0


let save_to_temp_file = fun prefix suffix (content: string) ->
  let filename, ch = Filename.open_temp_file prefix suffix in
  let _ = output_string ch content in
  let _ = close_out ch in
  filename


exception No_such_file of string

let assert_file_exists = fun f ->
  if not (Sys.file_exists f) then raise (No_such_file f)


let now = Unix.time


let rfc3339_now = fun () ->
  let t = Unix.gettimeofday () in
  Netdate.mk_internet_date ~digits:3 t


let valid_email = fun str ->
  try
    let l = Netaddress.parse str in
    let _ = assert (List.length l == 1) in
    let addr = List.hd l in
    ( match addr with
    | `Group _ -> false
    | `Mailbox m -> true
    )
  with
    Netaddress.Parse_error _
  | Assert_failure _ -> false


exception Todo of string

let todo_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let url = Neturl.parse_url (cgi # url ()) in
  let service_name = String.concat "" (Neturl.url_path url) in
  raise (Todo service_name)


let string_of_file = fun f ->
  let ic = open_in f in
  let n = in_channel_length ic in
  let s = String.create n in
  really_input ic s 0 n;
  close_in ic;
  s


let logline = fun filename msg ->
  try
    let cout = open_out_gen [Open_wronly; Open_creat; Open_append; Open_text] 0o644 filename in
    let timestamp = rfc3339_now () in
    let line = Format.sprintf "[camelis server] %s -- %s\n" timestamp msg in
    let _ = output_string cout line in
    close_out cout
  with Sys_error _ as e ->
    Format.printf "Cannot open file \"%s\": %s\n" filename (Printexc.to_string e)


(* ----------------------------------------------------------------- *)
(* About lists *)

(* This code taken from ExtLib, see http://code.google.com/p/ocaml-extlib/ *)
type 'a mut_list = {
  hd : 'a;
  mutable tl : 'a list;
}
external inj : 'a mut_list -> 'a list = "%identity"
let dummy_node () = { hd = Obj.magic (); tl = [] }

(* take n elements in list l, starting at position start*)
let take n l =
  let rec loop n dst = function
    | h :: t when n > 0 ->
      let r = { hd = h; tl = [] } in
      dst.tl <- inj r;
      loop (n - 1) r t
    | _ ->
      ()
  in
  let dummy = dummy_node () in
  loop n dummy l;
  dummy.tl

(* drop n first elements *)
let rec drop = fun n l ->
  match l with
    | _ :: l' when n > 0 -> drop (n - 1) l'
    | l' -> l'


(* takewhile and dropwhile by Richard W.M. Jones. *)
let rec takewhile = fun f l ->
  match l with
    | [] -> []
    | x :: xs when f x -> x :: (takewhile f xs)
    | _ -> []

let rec dropwhile = fun f l ->
  match l with
    | [] -> []
    | x :: xs when f x -> dropwhile f xs
    | list -> list
