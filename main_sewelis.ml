open Nethttpd_types
open Nethttpd_services
open Nethttpd_engine
open Utils
open Xmlutils
open StdLabels
open Users

module Sewelis = Func_api

(* -------------------------------------------------------------- *)
(* Globals *)
let port = ref 0

let activation_date = ref (rfc3339_now ())
let last_update = ref "none"

let logfile = ref "/tmp/portalis.log"
let ignore_rights = ref false

let store_dir = ref "/tmp"



(* -------------------------------------------------------------- *)
(* exception handling *)

type param_error = Missing_parameter of string | Empty_parameter of string
exception Parameter_error of param_error list
exception Not_a_valid_order of string
exception Not_a_valid_aggreg of string
exception Not_a_valid_transformation of string

let xml_of_exn = fun e ->
  let xml_msg msg = [Xml.Element ("message", [], [pcdata msg])] in
  match e with
  | Parameter_error errors ->
    let f = fun err -> match err with
      | Missing_parameter str -> xml_msg ("parameter " ^ str ^ " is missing")
      | Empty_parameter str -> xml_msg ("parameter " ^ str ^ " is empty")
    in
    List.flatten (List.map f errors)
  | No_such_file str -> xml_msg ("File " ^ str ^ " was not found on the server.")
  | Not_a_valid_order str -> xml_msg (str ^ "does not represent a column order. Use one of \"default\", \"asc\" or \"desc\".")
  | Not_a_valid_aggreg str -> xml_msg (str ^ "does not represent an aggregation. Use one of \"index\", \"distinct_count\", \"sum\" or \"avg\".")
  | Not_a_valid_transformation str -> xml_msg (str ^ "does not represent a transformation.")
  | Todo str -> xml_msg ("TODO: implement " ^ str ^ " command.")
  | Not_a_valid_rdf_term str -> xml_msg (str ^ " is not a valid XML representation of an RDF term.")
  | _ -> xml_msg (Printexc.to_string e)



(* -------------------------------------------------------------- *)
(* Common sanity checks *)

let check_param = fun param (cgi: Netcgi.cgi) ->
  if not (cgi # argument_exists param) then
    Some (Missing_parameter param)
  else if (cgi # argument_value param) = "" then
    Some (Empty_parameter param)
  else
    None

let assert_params = fun params (cgi: Netcgi.cgi) ->
  let checks = List.map (fun p -> check_param p cgi) params in
  let errors = List.filter (fun x -> is_some x) checks in
  let errors' = List.map val_of_option errors in
  if errors' <> [] then raise (Parameter_error errors')



(* -------------------------------------------------------------- *)
(* Stores > Store creators *)

(* No service associated with save_store, since it is automatically
   called after an operation that modifies the store *)
let save_store = fun store ->
  Sewelis.save_store ~store


let create_store_service = fun tag (cgi: Netcgi.cgi) ->
  let filename = string_argument cgi "filename" in
  let full_filename = !store_dir // filename in
  let id = Sewelis.create_store full_filename in
  let _ = save_store id in
  xml_ok tag ~attrs:["storeId", string_of_int id] ~children:[]


let load_store_service = fun tag (cgi: Netcgi.cgi) ->
  let filename = string_argument cgi "filename" in
  let full_filename = !store_dir // filename in
  let id = Sewelis.load_store full_filename in
  let _ = save_store id in
  xml_ok tag ~attrs:["storeId", string_of_int id] ~children:[]


let export_rdf_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  (* TODO: check file suffix is ttl *)
  (* TODO: rename to exportTurtle? *)
  let filename = string_argument cgi "filename" in
  let filepath = !store_dir // filename in
  let base = Sewelis.store_base ~store in
  let _ = Sewelis.export_rdf ~store ~base ~xmlns:[] ~filepath in
  xml_ok tag



(* -------------------------------------------------------------- *)
(* Stores > Store accessors *)

let store_base_service = fun tag (cgi:Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let uri = Sewelis.store_base ~store in
  let attrs = ["uri", uri] in
  xml_ok tag ~attrs

let store_xmlns_service = fun tag (cgi:Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let namespaces = Sewelis.store_xmlns ~store in
  xml_ok tag ~children:(List.map xml_of_ns namespaces)

let uri_description_service = fun tag (cgi:Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let uri = string_argument cgi "uri" in
  let display = Sewelis.uri_description ~store ~uri in
  let children = [xml_of_display display] in
  xml_ok tag ~children



(* -------------------------------------------------------------- *)
(* Stores > Store modifiers *)

let define_base_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let base = string_argument cgi "base" in
  let _ = Sewelis.define_base ~store ~base in
  xml_ok tag

let define_namespace_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let prefix = string_argument cgi "prefix" in
  let uri = string_argument cgi "uri" in
  let _ = Sewelis.define_namespace ~store ~prefix ~uri in
  xml_ok tag

let add_triple_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let s = string_argument cgi "s" in
  let p = string_argument cgi "p" in
  let o = rdf_of_xmlstring (string_argument cgi "o") in
  let _ = Sewelis.add_triple ~store ~s ~p ~o in
  xml_ok tag

let remove_triple_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let s = string_argument cgi "s" in
  let p = string_argument cgi "p" in
  let o = rdf_of_xmlstring (string_argument cgi "o") in
  let _ = Sewelis.remove_triple ~store ~s ~p ~o in
  xml_ok tag

let replace_object_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let s = string_argument cgi "s" in
  let p = string_argument cgi "p" in
  let o = rdf_of_xmlstring (string_argument cgi "o") in
  let _ = Sewelis.replace_object ~store ~s ~p ~o in
  xml_ok tag

let import_rdf_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let base = (string_argument cgi "base") in
  let filepath = !store_dir // (string_argument cgi "filename") in
  let _ = Sewelis.import_rdf ~store ~base ~filepath in
  xml_ok tag

let import_uri_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let uri = string_argument cgi "uri" in
  let _ = Sewelis.import_uri ~store ~uri in
  xml_ok tag



(* -------------------------------------------------------------- *)
(* Navigation places > Place creators *)

type kind_of_place = Root | Home | Bookmarks | Drafts | Uri of string | Statement of string

let get_place = fun place_kind tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let get_place_function = match place_kind with
    | Root -> Sewelis.get_place_root
    | Home -> Sewelis.get_place_home
    | Bookmarks -> Sewelis.get_place_bookmarks
    | Drafts -> Sewelis.get_place_drafts
    | Uri uri -> Sewelis.get_place_uri ~uri
    | Statement statement -> Sewelis.get_place_statement ~statement in
  let place = get_place_function ~store in
  let statement_str = Sewelis.statement_string ~store ~place in
  let display = Sewelis.statement_display ~store ~place in
  let xml_display = xml_of_display display.display in
  xml_ok tag
    ~attrs:[ "placeId", string_of_int place;
             "statement", statement_str ]
    ~children:[xml_display]

let get_place_root_service = get_place Root
let get_place_home_service = get_place Home
let get_place_bookmarks_service = get_place Bookmarks
let get_place_drafts_service = get_place Drafts

let get_place_uri_service = fun tag (cgi: Netcgi.cgi) ->
  let uri = string_argument cgi "uri" in
  get_place (Uri uri) tag cgi

let get_place_statement_service = fun tag (cgi: Netcgi.cgi) ->
  let statement = string_argument cgi "statement" in
  get_place (Statement statement) tag cgi



(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Place statement *)

let statement_string_service = fun tag (cgi:Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let attrs = ["value", Sewelis.statement_string ~store ~place] in
  xml_ok tag ~attrs

let statement_display_service = fun tag (cgi:Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let fdisplay = Sewelis.statement_display ~store ~place in
  let focus, display = fdisplay.focus, fdisplay.display in
  let attrs = ["focus", string_of_int focus] in
  let children = [xml_of_display display] in
  xml_ok tag ~attrs ~children



(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Statement relaxation *)

let statement_relaxation_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let rank = Sewelis.rank ~place in
  let has_more = Sewelis.has_more ~place in
  let has_less = Sewelis.has_less ~place in
  let attrs = ["rank", string_of_int rank;
               "hasMore", string_of_bool has_more;
               "hasLess", string_of_bool has_less] in
  xml_ok tag ~attrs ~children:[]



(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Place answers *)

let answer_counts_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let count = Sewelis.answers_count place in
  xml_ok tag ~attrs:["count", string_of_int count]

let answers_page_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let page_start = Sewelis.answers_page_start place in
  let page_end = Sewelis.answers_page_end place in
  let page_size = Sewelis.answers_page_size place in
  xml_ok tag ~attrs:["pageStart", string_of_int page_start;
                     "pageEnd", string_of_int page_end;
                     "pageSize", string_of_int page_size]

let answers_columns_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let columns = Sewelis.answers_columns place in
  let xml_columns = List.map (xml_of_column place) columns in
  xml_ok tag ~attrs:[] ~children:xml_columns

let get_cells_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let cell_rows = Sewelis.get_cells ~place in
  let xml_rows = List.map xml_of_cell_row cell_rows in
  xml_ok tag ~attrs:[] ~children:xml_rows

let cell_display_service = fun tag (cgi:Netcgi.cgi) ->
  let cell = int_argument cgi "cellId" in
  let display = Sewelis.cell_display cell in
  let children = [xml_of_display display] in
  xml_ok tag ~children



(* -------------------------------------------------------------- *)
(* Navigation places > Place modifiers > Statement relaxation *)

let show_more_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_more ~place in
  xml_ok tag

let show_less_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_less ~place in
  xml_ok tag

let show_most_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_most ~place in
  xml_ok tag

let show_least_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_least ~place in
  xml_ok tag



(* -------------------------------------------------------------- *)
(* Navigation places > Place modifiers > Place answers *)

let page_down_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_down ~place in
  let attrs = ["value", string_of_bool result] in
  xml_ok tag ~attrs

let page_up_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_up ~place in
  let attrs = ["value", string_of_bool result] in
  xml_ok tag ~attrs

let page_top_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_top ~place in
  let attrs = ["value", string_of_bool result] in
  xml_ok tag ~attrs

let page_bottom_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_bottom ~place in
  let attrs = ["value", string_of_bool result] in
  xml_ok tag ~attrs

let set_page_start_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let pos = int_argument cgi "pos" in
  let result = Sewelis.set_page_start ~place ~pos in
  let attrs = ["value", string_of_bool result] in
  xml_ok tag ~attrs

let set_page_end_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let pos = int_argument cgi "pos" in
  let result = Sewelis.set_page_end ~place ~pos in
  let attrs = ["value", string_of_bool result] in
  xml_ok tag ~attrs

let move_column_left_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let _ = Sewelis.move_column_left ~place ~column in
  xml_ok tag

let move_column_right_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let _ = Sewelis.move_column_right ~place ~column in
  xml_ok tag

let set_column_hidden_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let hidden = bool_argument cgi "hidden" in
  let _ = Sewelis.set_column_hidden ~place ~column ~hidden in
  xml_ok tag

let set_column_order_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let order = match (string_argument cgi "order") with
    | "default" -> `DEFAULT
    | "desc" -> `DESC
    | "asc" -> `ASC
    | x -> raise (Not_a_valid_order x) in
  let _ = Sewelis.set_column_order ~place ~column ~order in
  xml_ok tag

let set_column_pattern_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let pattern = string_argument cgi "pattern" in
  let _ = Sewelis.set_column_pattern ~place ~column ~pattern in
  xml_ok tag

let set_column_aggreg_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let aggreg = match (string_argument cgi "aggreg") with
    | "index" -> `INDEX
    | "distinct_count" -> `DISTINCT_COUNT
    | "sum" -> `SUM
    | "avg" -> `AVG
    | x -> raise (Not_a_valid_aggreg x) in
  let _ = Sewelis.set_column_aggreg ~place ~column ~aggreg in
  xml_ok tag



(* -------------------------------------------------------------- *)
(* Navigation places > Place-based store modifiers *)

let do_assert_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.do_assert ~place in
  xml_ok tag

let do_retract_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.do_retract ~place in
  xml_ok tag

let set_as_home_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.set_as_home ~place in
  xml_ok tag

let add_as_bookmark_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.add_as_bookmark ~place in
  xml_ok tag

let add_as_draft_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.add_as_draft ~place in
  xml_ok tag

let remove_as_draft_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.remove_as_draft ~place in
  xml_ok tag



(* -------------------------------------------------------------- *)
(* Increments > Increment creators *)

let get_increment_entity_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let increment = Sewelis.get_increment_entity ~place in
  xml_ok tag ~attrs:["increment", string_of_int increment]

let get_increment_relation_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let increment = Sewelis.get_increment_relation ~place in
  xml_ok tag ~attrs:["increment", string_of_int increment]

let get_children_increments_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let parent = int_argument cgi "parentId" in
  let increments = Sewelis.get_children_increments ~place ~parent in
  let children = List.map xml_of_increment increments in
  xml_ok tag ~children

let get_increment_tree_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let parent = int_argument cgi "parentId" in
  let increments = Sewelis.get_increment_tree ~place ~parent in
  let children = [xml_of_increment_tree increments] in
  xml_ok tag ~children

let get_completions_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let key = string_argument cgi "matchingKey" in
  let max_compl =
    if (cgi # argument_exists "maxCompl") then
      Some (int_argument cgi "maxCompl")
    else None in
  let nb_more_relax =
    if (cgi # argument_exists "nbMoreRelax") then
      Some (int_argument cgi "nbMoreRelax")
    else None in
  let completions = Sewelis.get_completions ~place ~key ?max_compl ?nb_more_relax in
  xml_ok tag ~children:[xml_of_completions completions]



(* -------------------------------------------------------------- *)
(* Increments > Increment accessors *)

let string_of_kind = fun k ->
  match k with
  | Lisql_feature.Kind_Something -> "something"
  | Lisql_feature.Kind_Variable -> "variable"
  | Lisql_feature.Kind_Entity -> "entity"
  | Lisql_feature.Kind_Literal -> "literal"
  | Lisql_feature.Kind_Thing -> "thing"
  | Lisql_feature.Kind_Class -> "class"
  | Lisql_feature.Kind_Operator -> "operator"
  | Lisql_feature.Kind_Property -> "property"
  | Lisql_feature.Kind_InverseProperty -> "inverseProperty"
  | Lisql_feature.Kind_Structure -> "structure"
  | Lisql_feature.Kind_Argument -> "argument"

let increment_kind_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let kind = Sewelis.increment_kind ~increment in
  xml_ok tag ~attrs:["kind", string_of_kind kind]

let increment_uri_opt_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let uri = match Sewelis.increment_uri_opt ~increment with
    | None -> "none"
    | Some u -> u in
  xml_ok tag ~attrs:["uri", uri]

let increment_term_opt_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  match Sewelis.increment_term_opt ~increment with
  | None -> xml_ok tag
  | Some term -> xml_ok tag ~children:[xml_of_term term]


let increment_display_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let display = Sewelis.increment_display ~increment in
  xml_ok tag ~children:[xml_of_display display]


let increment_ratio_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let antecedent, consequent = Sewelis.increment_ratio ~increment in
  xml_ok tag ~attrs:["antecedent", string_of_int antecedent;
                     "consequent", string_of_int consequent]


let increment_new_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let is_new = Sewelis.increment_new ~increment in
  xml_ok tag ~attrs:["isNew", string_of_bool is_new]


let increment_info_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let kind = Sewelis.increment_kind ~increment in
  let uri = match Sewelis.increment_uri_opt ~increment with
    | None -> "none"
    | Some u -> u in
  let term = match Sewelis.increment_term_opt ~increment with
  | None -> []
  | Some term -> [xml_of_term term] in
  let display = xml_of_display $ Sewelis.increment_display ~increment in
  let antecedent, consequent = Sewelis.increment_ratio ~increment in
  let is_new = Sewelis.increment_new ~increment in
  xml_ok tag ~attrs:["uri", uri;
                     "kind", string_of_kind kind;
                     "antecedent", string_of_int antecedent;
                     "consequent", string_of_int consequent;
                     "isNew", string_of_bool is_new]
         ~children:(display::term)



(* -------------------------------------------------------------- *)
(* Statement transformation / Navigation links *)

let get_transformations_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let transf_list = Sewelis.get_transformations ~place in
  let transformations = Xml.Element ("transformations", [],
                                     List.map xml_of_transformation transf_list) in
  xml_ok tag ~children:[transformations]


let can_insert_entity_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let can_insert = string_of_bool (Sewelis.can_insert_entity ~place) in
  xml_ok tag ~attrs:["canInsert", can_insert]


let can_insert_relation_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let can_insert = string_of_bool (Sewelis.can_insert_relation ~place) in
  xml_ok tag ~attrs:["canInsert", can_insert]


let change_focus_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let focus = int_argument cgi "focusId" in
  let new_place = Sewelis.change_focus ~store ~place ~focus in
  xml_ok tag ~attrs:["placeId", string_of_int new_place]


let transformation_of_xml = fun s ->
  let xml = Xml.parse_string (url_decode s) in
  let open Lisql.Transf in
  match xml with
  | Xml.Element ("FocusUp", [], []) -> FocusUp
  | Xml.Element ("FocusDown", [], []) -> FocusDown
  | Xml.Element ("FocusLeft", [], []) -> FocusLeft
  | Xml.Element ("FocusRight", [], []) -> FocusRight
  | Xml.Element ("FocusTab", [], []) -> FocusTab
  | Xml.Element ("InsertForEach", [], []) -> InsertForEach
  | Xml.Element ("InsertCond", [], []) -> InsertCond
  | Xml.Element ("InsertSeq", [], []) -> InsertSeq
  | Xml.Element ("InsertAnd", [], []) -> InsertAnd
  | Xml.Element ("InsertOr", [], []) -> InsertOr
  | Xml.Element ("InsertAndNot", [], []) -> InsertAndNot
  | Xml.Element ("InsertAndMaybe", [], []) -> InsertAndMaybe
  | Xml.Element ("InsertIsThere", [], []) -> InsertIsThere
  | Xml.Element ("ToggleQu", [], [Xml.Element ("An", [], [])]) -> ToggleQu An
  | Xml.Element ("ToggleQu", [], [Xml.Element ("The", [], [])]) -> ToggleQu The
  | Xml.Element ("ToggleQu", [], [Xml.Element ("Every", [], [])]) -> ToggleQu Every
  | Xml.Element ("ToggleQu", [], [Xml.Element ("Only", [], [])]) -> ToggleQu Only
  | Xml.Element ("ToggleQu", [], [Xml.Element ("No", [], [])]) -> ToggleQu No
  | Xml.Element ("ToggleQu", [], [Xml.Element ("Each", [], [])]) -> ToggleQu Each
  | Xml.Element ("ToggleNot", [], []) -> ToggleNot
  | Xml.Element ("ToggleMaybe", [], []) -> ToggleMaybe
  | Xml.Element ("ToggleOpt", [], []) -> ToggleOpt
  | Xml.Element ("ToggleTrans", [], []) -> ToggleTrans
  | Xml.Element ("ToggleSym", [], []) -> ToggleSym
  | Xml.Element ("Describe", [], []) -> Describe
  | Xml.Element ("Select", [], []) -> Select
  | Xml.Element ("Delete", [], []) -> Delete
  | _ -> raise (Not_a_valid_transformation s)

let apply_transformation_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let transformation = transformation_of_xml (string_argument cgi "transformation") in
  let new_place = Sewelis.apply_transformation ~store ~place ~transformation in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_increment_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let increment = int_argument cgi "incrementId" in
  let new_place = Sewelis.insert_increment ~store ~place ~increment in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]

let insert_increment_list = fun combinator tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let increment_list = List.map (fun x -> int_of_string x#value)
                            (cgi # multiple_argument "incrementId") in
  let insert_func = match combinator with
    | `And -> Sewelis.insert_increment_list_and
    | `Or -> Sewelis.insert_increment_list_or
    | `Not -> Sewelis.insert_increment_list_not in
  let new_place = insert_func ~store ~place ~increment_list in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]

let insert_increment_list_and_service = insert_increment_list `And
let insert_increment_list_or_service = insert_increment_list `Or
let insert_increment_list_not_service = insert_increment_list `Not


let unquote_increment_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let increment = int_argument cgi "incrementId" in
  let new_place = Sewelis.unquote_increment ~store ~place ~increment in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_plain_literal_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let text = string_argument cgi "text" in
  let lang = string_argument cgi "lang" in
  let new_place = Sewelis.insert_plain_literal ~store ~place ~text ~lang in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_typed_literal_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let text = string_argument cgi "text" in
  let datatype = string_argument cgi "datatype" in
  let new_place = Sewelis.insert_typed_literal ~store ~place ~text ~datatype in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_date_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let year = int_argument cgi "year" in
  let month = int_argument cgi "month" in
  let day = int_argument cgi "day" in
  let new_place = Sewelis.insert_date ~store ~place ~year ~month ~day in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_dateTime_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let year = int_argument cgi "year" in
  let month = int_argument cgi "month" in
  let day = int_argument cgi "day" in
  let hours = int_argument cgi "hours" in
  let minutes = int_argument cgi "minutes" in
  let seconds = int_argument cgi "seconds" in
  let new_place = Sewelis.insert_dateTime ~store ~place
                                      ~year ~month ~day
                                      ~hours ~minutes ~seconds in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_filename_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let filename = string_argument cgi "filename" in
  let new_place = Sewelis.insert_filename ~store ~place ~filename in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_var_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let varname' = string_argument cgi "varname" in
  let varname = if varname' = "" then None else Some varname' in
  let new_place = Sewelis.insert_var ~store ~place ~varname in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_uri_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_uri ~store ~place ~uri in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_class_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_class ~store ~place ~uri in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_property_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_property ~store ~place ~uri in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_inverse_property_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_inverse_property ~store ~place ~uri in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]


let insert_structure_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let arity = int_argument cgi "arity" in
  let index = int_argument cgi "index" in
  let new_place = Sewelis.insert_structure ~store ~place ~uri ~arity ~index in
  xml_ok tag ~attrs:["newPlace", string_of_int new_place]



(* -------------------------------------------------------------- *)
(* Freeing memory *)

let free_focus_service = fun tag (cgi: Netcgi.cgi) ->
  let focus = int_argument cgi "focusId" in
  let _ = Sewelis.free_focus ~focus in
  xml_ok tag

let free_cell_service = fun tag (cgi: Netcgi.cgi) ->
  let cell = int_argument cgi "cellId" in
  let _ = Sewelis.free_cell ~cell in
  xml_ok tag

let free_increment_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let _ = Sewelis.free_increment ~increment in
  xml_ok tag

let free_place_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.free_place ~place in
  xml_ok tag

let free_store_service = fun tag (cgi: Netcgi.cgi) ->
  let store = int_argument cgi "storeId" in
  let _ = Sewelis.free_store ~store in
  xml_ok tag

let free_root_service = fun tag (cgi: Netcgi.cgi) ->
  let _ = Sewelis.free_root () in
  xml_ok tag



(* -------------------------------------------------------------- *)
(* Compute and send result to HTTP queries *)

let ping_service = fun tag cgi ->
  let attrs = [ "activationDate", !activation_date;
                "pid", string_of_int $ Unix.getpid ();
              ] in
  xml_ok tag ~attrs


let ping_users_service = fun tag (cgi:Netcgi.cgi) ->
  let xml_users = List.map xml_of_user (user_list ()) in
  xml_ok tag ~children:xml_users


let set_role_service = fun tag (cgi: Netcgi.cgi) ->
  let admin_key = string_argument cgi "adminKey" in
  let user_key = string_argument cgi "userKey" in
  let new_role_str = string_argument cgi "role" in
  let _ = assert_rights admin_key Admin in
  let new_role = role_of_string new_role_str in
  let _ = update_user_role user_key new_role in
  xml_ok tag ~children: [xml_of_key user_key]


let register_key_service = fun tag (cgi: Netcgi.cgi) ->
  let admin_key = string_argument cgi "adminKey" in
  let user_key = string_argument cgi "userKey" in
  let timeout = if cgi # argument_exists "timeout" then
      float_of_string (string_argument cgi "timeout")
    else
      default_key_timeout in
  let _ = assert_new_key user_key in
  let new_role_str = string_argument cgi "role" in
  let _ = assert_rights admin_key Admin in
  let new_role = role_of_string new_role_str in
  let _ = add_user user_key new_role (Some timeout) in
  xml_ok tag ~children: [xml_of_key user_key]


let delkey_service = fun tag (cgi: Netcgi.cgi) ->
  let admin_key = string_argument cgi "adminKey" in
  let user_key = string_argument cgi "userKey" in
  let _ = assert_rights admin_key Admin in
  let _ = remove_user user_key in
  xml_ok tag


(* -------------------------------------------------------------- *)
(* Dispatch services to URLs *)

(* list of services; each is a 5-uplet:
 *   URL,
 *   handler function,
 *   minimum role,
 *   list of required params,
 *   UpdateStore | UpdateUsers | NoChange
 *)
(* Note that presence of a "userKey" parameter is automatically checked
   if role <> No_right *)

type effect = UpdateStore | UpdateUsers | NoChange

let services = [ "/ping", ping_service, No_right, [], NoChange;
                 "/pingUsers", ping_users_service, Admin, [], NoChange;
                 "/setRole", set_role_service, No_right, ["adminKey"; "userKey"; "role"], UpdateUsers;
                 "/registerKey", register_key_service, No_right, ["adminKey"; "userKey"], UpdateUsers;
                 "/delKey", delkey_service, No_right, ["adminKey"; "userKey"], UpdateUsers;

                 (* Stores > Store creators *)
                 "/createStore", create_store_service, Admin, ["filename"], NoChange;
                 "/loadStore", load_store_service, Admin, ["filename"], NoChange;
                 "/exportRdf", export_rdf_service, Reader, ["storeId"; "filename"], NoChange;
                 (* Stores > Store accessors *)
                 "/storeBase", store_base_service, Reader, ["storeId"], NoChange;
                 "/storeXmlns", store_xmlns_service, Reader, ["storeId"], NoChange;
                 "/uriDescription", uri_description_service, Reader, ["storeId"; "uri"], NoChange;
                 (* Stores > Store modifiers *)
                 "/defineBase", define_base_service, Publisher, ["storeId"], UpdateStore;
                 "/defineNamespace", define_namespace_service, Publisher, ["storeId"; "prefix"; "uri"], UpdateStore;
                 "/addTriple", add_triple_service, Publisher, ["storeId"; "s"; "p"; "o"], UpdateStore;
                 "/removeTriple", remove_triple_service, Publisher, ["storeId"; "s"; "p"; "o"], UpdateStore;
                 "/replaceObject", replace_object_service, Publisher, ["storeId"; "s"; "p"; "o"], UpdateStore;
                 "/importRdf", import_rdf_service, Admin, ["storeId"; "base"; "filename"], UpdateStore;
                 "/importUri", import_uri_service, Publisher, ["storeId"; "uri"], UpdateStore;

                 (* Navigation places > Place creators *)
                 "/getPlaceRoot", get_place_root_service, Reader, ["storeId"], NoChange;
                 "/getPlaceHome", get_place_home_service, Reader, ["storeId"], NoChange;
                 "/getPlaceBookmarks", get_place_bookmarks_service, Reader, ["storeId"], NoChange;
                 "/getPlaceDrafts", get_place_drafts_service, Reader, ["storeId"], NoChange;
                 "/getPlaceUri", get_place_uri_service, Reader, ["storeId"; "uri"], NoChange;
                 "/getPlaceStatement", get_place_statement_service, Reader, ["storeId"; "statement"], NoChange;

                 (* Navigation places > Place accessors > Place statement *)
                 "/statementString", statement_string_service, Reader, ["storeId"; "placeId"], NoChange;
                 "/statementDisplay", statement_display_service, Reader, ["storeId"; "placeId"], NoChange;
                 (* Navigation places > Place accessors > Statement relaxation *)
                 "/statementRelaxation", statement_relaxation_service, Reader, ["placeId"], NoChange;
                 (* Navigation places > Place accessors > Place answers*)
                 "/answersCount", answer_counts_service, Reader, ["placeId"], NoChange;
                 "/answersPage", answers_page_service, Reader, ["placeId"], NoChange;
                 "/answersColumns", answers_columns_service, Reader, ["placeId"], NoChange;
                 "/getCells", get_cells_service, Reader, ["placeId"], NoChange;
                 "/cellDisplay", cell_display_service, Reader, ["cellId"], NoChange;

                 (* Navigation places > Place modifiers > Statement relaxation *)
                 "/showMore", show_more_service, Reader, ["placeId"], NoChange;
                 "/showLess", show_less_service, Reader, ["placeId"], NoChange;
                 "/showMost", show_most_service, Reader, ["placeId"], NoChange;
                 "/showLeast", show_least_service, Reader, ["placeId"], NoChange;
                 (* Navigation places > Place modifiers > Place answers *)
                 "/pageDown", page_down_service, Reader, ["placeId"], NoChange;
                 "/pageTop", page_top_service, Reader, ["placeId"], NoChange;
                 "/pageUp", page_up_service, Reader, ["placeId"], NoChange;
                 "/pageBottom", page_bottom_service, Reader, ["placeId"], NoChange;
                 "/setPageStart", set_page_start_service, Reader, ["placeId"; "pos"], NoChange;
                 "/setPageEnd", set_page_end_service, Reader, ["placeId"; "pos"], NoChange;
                 "/moveColumnLeft", move_column_left_service, Reader, ["placeId"; "column"], NoChange;
                 "/moveColumnRight", move_column_right_service, Reader, ["placeId"; "column"], NoChange;
                 "/setColumnHidden", set_column_hidden_service, Reader, ["placeId"; "column"; "hidden"], NoChange;
                 "/setColumnOrder", set_column_order_service, Reader, ["placeId"; "column"; "order"], NoChange;
                 "/setColumnPattern", set_column_pattern_service, Reader, ["placeId"; "column"; "pattern"], NoChange;
                 "/setColumnAggreg", set_column_aggreg_service, Reader, ["placeId"; "column"; "aggreg"], NoChange;

                 (* Navigation places > Place-based store modifiers *)
                 "/doAssert", do_assert_service, Publisher, ["placeId"], UpdateStore;
                 "/doRetract", do_retract_service, Publisher, ["placeId"], UpdateStore;
                 "/setAsHome", set_as_home_service, Publisher, ["placeId"], UpdateStore;
                 "/addAsBookmark", add_as_bookmark_service, Publisher, ["placeId"], UpdateStore;
                 "/addAsDraft", add_as_draft_service, Publisher, ["placeId"], UpdateStore;
                 "/removeAsDraft", remove_as_draft_service, Publisher, ["placeId"], UpdateStore;

                 (* Increments > Increment creators *)
                 "/getIncrementEntity", get_increment_entity_service, Reader, ["placeId"], NoChange;
                 "/getIncrementRelation", get_increment_relation_service, Reader, ["placeId"], NoChange;
                 "/getChildrenIncrements", get_children_increments_service, Reader, ["placeId"; "parentId"], NoChange;
                 "/getIncrementTree", get_increment_tree_service, Reader, ["placeId"; "parentId"], NoChange;
                 "/getCompletions", get_increment_entity_service, Reader, ["placeId"; "matchingKey"], NoChange;
                 (* Increments > Increment accessors *)
                 "/incrementKind", increment_kind_service, Reader, ["incrementId"], NoChange;
                 "/incrementUriOpt", increment_uri_opt_service, Reader, ["incrementId"], NoChange;
                 "/incrementTermOpt", increment_term_opt_service, Reader, ["incrementId"], NoChange;
                 "/incrementDisplay", increment_display_service, Reader, ["incrementId"], NoChange;
                 "/incrementRatio", increment_ratio_service, Reader, ["incrementId"], NoChange;
                 "/incrementNew", increment_new_service, Reader, ["incrementId"], NoChange;
                 "/incrementInfo", increment_info_service, Reader, ["incrementId"], NoChange;

                 (* Statement transformation / Navigation links *)
                 "/getTransformations", get_transformations_service, Reader, ["placeId"], NoChange;
                 "/canInsertEntity", can_insert_entity_service, Reader, ["placeId"], NoChange;
                 "/canInsertRelation", can_insert_relation_service, Reader, ["placeId"], NoChange;
                 "/changeFocus", change_focus_service, Reader, ["storeId"; "placeId"; "focusId"], NoChange;
                 "/applyTransformation", apply_transformation_service, Reader, ["storeId"; "placeId"; "transformation"], UpdateStore;
                 "/insertIncrement", insert_increment_service, Reader, ["storeId"; "placeId"; "incrementId"], UpdateStore;
                 "/insertIncrementListAnd", insert_increment_list_and_service, Reader, ["storeId"; "placeId"; "incrementId"], UpdateStore; (* multiple incrementId *)
                 "/insertIncrementListOr", insert_increment_list_or_service, Reader, ["storeId"; "placeId"; "incrementId"], UpdateStore; (* multiple incrementId *)
                 "/insertIncrementListNot", insert_increment_list_not_service, Reader, ["storeId"; "placeId"; "incrementId"], UpdateStore; (* multiple incrementId *)
                 "/unquoteIncrement", unquote_increment_service, Reader, ["storeId"; "placeId"; "incrementId"], UpdateStore;
                 "/insertPlainLiteral", insert_plain_literal_service, Reader, ["storeId"; "placeId"; "text"; "lang"], UpdateStore;

                 "/insertTypedLiteral", insert_typed_literal_service, Reader, ["storeId"; "placeId"; "text"; "datatype"], UpdateStore;
                 "/insertDate", insert_date_service, Reader, ["storeId"; "placeId"; "year"; "month"; "day"], UpdateStore;
                 "/insertDateTime", insert_dateTime_service, Reader, ["storeId"; "placeId"; "year"; "month"; "day"; "hours"; "minutes"; "seconds"], UpdateStore;
                 "/insertFilename", insert_filename_service, Reader, ["storeId"; "placeId"; "filename"], UpdateStore;
                 "/insertVar", insert_var_service, Reader, ["storeId"; "placeId"], UpdateStore; (* varname is optional *)
                 "/insertUri", insert_uri_service, Reader, ["storeId"; "placeId"; "uri"], UpdateStore;
                 "/insertClass", insert_class_service, Reader, ["storeId"; "placeId"; "uri"], UpdateStore;
                 "/insertProperty", insert_property_service, Reader, ["storeId"; "placeId"; "uri"], UpdateStore;
                 "/insertInverseProperty", insert_inverse_property_service, Reader, ["storeId"; "placeId"; "uri"], UpdateStore;
                 "/insertStructure", insert_structure_service, Reader, ["storeId"; "placeId"; "uri"; "arity"; "index"], UpdateStore;

                 (* Freeing memory *)
                 "/freeFocus", free_focus_service, Admin, ["focusId"], NoChange;
                 "/freeCell", free_cell_service, Admin, ["cellId"], NoChange;
                 "/freeIncrement", free_increment_service, Admin, ["incrementId"], NoChange;
                 "/freePlace", free_place_service, Admin, ["placeId"], NoChange;
                 "/freeStore", free_store_service, Admin, ["storeId"], NoChange;
                 "/freeRoot", free_root_service, Admin, [], NoChange;

               ]

(* Wrapper around service handlers *)
(* It checks for required params. It checks for access rights. It also
   catches exception fired by the handler, and send an xml
   representation of the error. *)
(* WARNING: the parameter for checking access rights is hardcoded to "userKey" *)
(* Also checks if the store has to be saved on disk *)
let wrap_handler = fun url handler role params update_kind env (cgi: Netcgi.cgi) ->
  (* remove leading "/" from url to get tagname *)
  let tag = (Str.string_after url 1) ^ "Response" in
  try
    if role <> No_right then begin
      assert_params ("userKey"::params) cgi;
      assert_rights (string_argument cgi "userKey") role
    end
    else
      assert_params params cgi;
    let _ = logline !logfile (cgi # url ~with_query_string:`Env ()) in
    let _ = update_user_expiration (string_argument cgi "userKey") in
    let xml = handler tag cgi in
    logline !logfile (url ^ " compute OK.");
    if update_kind = UpdateStore then
      let store_id = int_argument cgi "storeId" in
      let _ = last_update := rfc3339_now () in
      save_store store_id
    else ();
    send_xml env cgi xml;
    logline !logfile (url ^ " all done.")
  with
  | e ->
    let xml = xml_error tag (xml_of_exn e) in
    send_xml env cgi xml


let srv =
  let make_service = fun (url, f, role, params, update_kind) ->
    url,
    dynamic_service { dyn_handler = wrap_handler url f role params update_kind;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = Some "/";
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  uri_distributor $ List.map make_service services



(* -------------------------------------------------------------- *)
(* Boilerplate code to create and launch web server *)

open Nethttpd_reactor

let start port =
  let config = Nethttpd_reactor.default_http_reactor_config in
  let master_sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  Unix.setsockopt master_sock Unix.SO_REUSEADDR true;
  Unix.bind master_sock (Unix.ADDR_INET(Unix.inet_addr_any, port));
  Unix.listen master_sock 100;
  Printf.printf "Listening on port %d\n" port;
  flush stdout;

  while true do
    try
      let conn_sock, _ = Unix.accept master_sock in
      Unix.set_nonblock conn_sock;
      let _ = Thread.create (process_connection config conn_sock) srv in
      ()
    with
      Unix.Unix_error(Unix.EINTR,_,_) -> ()  (* ignore *)
  done



(* -------------------------------------------------------------- *)
(* Parse command-line arguments and launches server *)

let usage = String.concat " "
  [ "Usage:"; Sys.argv.(0);
    "-port portnum";
    "-key adminKey";
    "-storedir string";
    "[-log filename]";
    "[-despot]";
    "\n"
  ]

let speclist =
  let add_creator = fun k -> add_user k Admin None in
  Arg.align
    [ ("-port",      Arg.Set_int port,           " port number the server will listen");
      ("-key",       Arg.String add_creator,     " key of the admin who owns the service");
      ("-storedir",  Arg.Set_string store_dir,   " path to the directory holding store data on server");
      ("-log",       Arg.Set_string logfile,     " path to the log file (defaults to " ^ !logfile ^ ")");
      ("-despot",    Arg.Set ignore_rights,      " ignore users right (use for debugging purposes)");
    ]

let anon_fun = fun x -> raise $ Arg.Bad ("Bad argument : " ^ x)

let check_args = fun () ->
  let warn cond msg = if cond then Printf.printf "[Warning] %s\n" msg else () in
  let check cond msg = if cond then raise $ Arg.Bad msg else () in
  let in_argv str = array_mem str Sys.argv in
  let warnings = [ not (in_argv "-log"), "log not specified, defaulting to " ^ !logfile;
                 ] in
  let checks = [ not (in_argv "-port"), "Unspecified port number";
                 not (in_argv "-storedir"), "Unspecified storedir";
                 not (in_argv "-key"), "Unspecified key";
               ] in
  List.iter (uncurry check) checks;
  List.iter (uncurry warn) warnings

let main = fun () ->

let _ = Arg.parse speclist anon_fun usage in
  try
    check_args ();
    Netsys_signal.init ();
    start !port
  with Arg.Bad msg ->
    print_endline $ "Error: " ^ msg;
    print_newline ();
    Arg.usage speclist usage

let _ = if not !Sys.interactive then main ()
